Ce package contient un système d'armes et d'équipement.

---Utiliser des armes à distance---
-Ajouter un "Range Attack Manager" à un gameobject. 
-Indiquer le pool d'attaques que ce manager utilisera (nous reviendront au pool plus tard)
-La taille de l'inventaire peut actuellement être de 2 où 1, ce package ne supporte pas encore 
de plus gros inventaires.
-"Empty Weapon" représente l'arme "vide", qui correspond à ne pas avoir d'armes équipées.
-Pour attaquer avec l'arme actuelle, appeller la méthode "Attack".
-Les munitions descendent de 1 lorsqu'une attaque est lancée.

---Utiliser des armes de melée---
-Ajouter un "Melee Attack Manager" à un gameobject.
-La seule différence avec le "Range Attack Manager" est que la durabilité de l'arme ne descent pas
lorsqu'une attaque est effectuée, mais lorsque l'arme active collisionne contre un objet 
(une fois par attaque). Cette collision est enregistrée par le script "Melee Collision"

---Modifier l'équipement---
-Dans l'"Attack Manager", appeler "Add Weapon" suivi du nom de l'arme afin de remplacer l'arme 
actuelle par la nouvelle arme.
-Dans l'"Attack Manager", appeler "Remove Current Weapon" pour remplacer l'arme actuelle
par l'arme vide.

---Créer une arme---
-Créer un gameobject et lui ajouter un "WeaponData"
-Indiquer le nom de l'arme, le modèle de l'arme sur la scène, son temps de recharge et ses munitions.
-Pour chaque comportement de l'arme, ajouter un script héritant de "Weapon Behaviour" et le référencer
dans la liste des behaviours du "Weapon Data".

---Créer un pool d'armes---
-Ajouter un "Weapons Pool" à un gameobject
-Y référencer toutes les "WeaponData" du pool

---Créer un projectile---
-Sur un gameobject, ajouter un "Projectile Manager"
-Ajouter sur ce même objet des scripts héritant de "Projectile Behaviour" et les référencer
sur le manager

---Créer de nouveaux Weapons Behaviours---
-Créer un script C# qui hérite de Weapon Behaviour et override ses fonctions selon vos besoins
-OnInitialize est lancé une fois lorsque l'on active l'arme
-OnExecute est lancé lorsque l'on lance l'attaque
-OnWeaponActive est lancé continuellement tant que l'arme est active
-OnDeactivate est lancé une fois lorsque l'on change d'arme

---Créer de nouveaux Projectile Behaviours---
-Idem que pour les Weapon Behaviours
-OnInitialize est lancé lorsque le projectile est créé.
-OnUpdate se lance continuellement quand le projectile est actif 
-OnDisables se lance lorsque le projectile se désactive
-OnCollide se lance lorsque le projectile rencontre un obstacle