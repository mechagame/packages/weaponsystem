﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace WeaponSystem.EditorScripts
{
    [CustomEditor(typeof(WSProjectileManager))]
    public class WSProjectileManagerEditor : Editor
    {
        WSProjectileManager projectileManager;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            projectileManager = (WSProjectileManager)target;

            if(GUILayout.Button("Find Behaviours"))
            {
                projectileManager.FindBehaviours();
            }            
        }
    }
}
