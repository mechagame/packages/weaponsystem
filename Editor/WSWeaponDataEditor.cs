﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace WeaponSystem.EditorScripts
{
    [CustomEditor(typeof(WSWeaponData))]
    public class WSWeaponDataEditor : Editor
    {
        WSWeaponData weaponData;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            weaponData = (WSWeaponData)target;
            if(GUILayout.Button("Find Behaviours"))
            {
                weaponData.FindBehaviours();
            }
        }
    }
}
