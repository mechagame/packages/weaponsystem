﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace WeaponSystem.EditorScripts
{
    [CustomEditor(typeof(WSAttackPool))]
    public class WSAttackPoolEditor : Editor
    {
        WSAttackPool attackPool;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            attackPool = (WSAttackPool)target;
            if(GUILayout.Button("Find Attacks"))
            {
                attackPool.FindWeapons();
            }

        }
    }
}
