﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    public class WSCollectibleWeapon : MonoBehaviour
    {
        [SerializeField] private string weaponName;
        public string GetWeaponName => weaponName;

        public enum WeaponType { melee, range};
        [SerializeField] private WeaponType weaponType = WeaponType.range;
        public WeaponType GetWeaponType => weaponType;

        public UnityEvent onCollected;

        public void Collect()
        {
            onCollected?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
