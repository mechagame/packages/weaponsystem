﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSRangeAttackManager : WSAttackManager
    {
        protected override void RemoveAmmo()
        {
            currentWeapon.ammo--;
        }
    }
}
