﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    public abstract class WSAttackManager : MonoBehaviour
    {
        public UnityEvent onWeaponSwap;
        public event Action<List<WSInventoryWeapon>> onInventoryChange;

        private List<WSInventoryWeapon> inventory = new List<WSInventoryWeapon>();

        [SerializeField] private WSAttackPool attackPool;
        [SerializeField] private int inventorySize = 2;
        [SerializeField] private string emptyWeapon;
        [SerializeField] private string firstWeapon;
        [SerializeField] private string secondWeapon;

        protected float remainingCooldown = 0;

        protected WSInventoryWeapon currentWeapon => inventory[0];

        private void Start()
        {
            if (firstWeapon == string.Empty) { AddWeapon(emptyWeapon); AddWeapon(emptyWeapon); return; }
            AddWeapon(firstWeapon);
            if (secondWeapon == string.Empty) { AddWeapon(emptyWeapon); SwapWeapons(); return; }
            AddWeapon(secondWeapon);
            SwapWeapons();
        }

        private void Update()
        {
            UpdateCurrentWeapon();
            Cooldown();
        }


        #region AttackManager

        public void Attack()
        {
            if (remainingCooldown != 0) { return; }
            if (currentWeapon.ammo == 0)
            {
                if (inventory[1].ammo == 0)
                {
                    onInventoryChange?.Invoke(inventory);
                    return;
                }
                else
                {
                    RemoveCurrentWeapon();
                    return;
                }
            }
            currentWeapon.weapon.Attack();
            RemoveAmmo();
            remainingCooldown = currentWeapon.weapon.GetCooldown;
            onInventoryChange?.Invoke(inventory);
        }

        public void AttackPressDown()
        {
            currentWeapon.weapon.InputHold();
        }

        public void AttackPressUp()
        {
            currentWeapon.weapon.InputRelease();
        }

        protected abstract void RemoveAmmo();

        protected void RemoveBullet()
        {
            currentWeapon.ammo--;
            onInventoryChange?.Invoke(inventory);
        }

        private void Cooldown()
        {
            remainingCooldown -= Time.deltaTime;
            remainingCooldown = Mathf.Max(remainingCooldown, 0);
        }
        private void UpdateCurrentWeapon()
        {
            currentWeapon.weapon.UpdateBehaviour();
        }


        #endregion

        #region Inventory
        public void AddWeapon(string weaponName)
        {
            WSWeaponData weapon = attackPool.GetWeaponsPool[weaponName];
            WSInventoryWeapon newWeapon = new WSInventoryWeapon(weapon);

            if (inventory.Count < inventorySize)
            {
                inventory.Add(newWeapon);
                currentWeapon.weapon.DeactivateWeapon();
                inventory.Reverse();
                currentWeapon.weapon.Initialize();
            }
            else
            {
                if (inventory[1].weapon.GetWeaponName == emptyWeapon) { SwapWeapons(); }
                if (inventory[1].ammo == 0) { SwapWeapons(); }
                currentWeapon.weapon.DeactivateWeapon();
                inventory[0] = newWeapon;
                currentWeapon.weapon.Initialize();
            }

            onInventoryChange?.Invoke(inventory);
        }

        public void RemoveCurrentWeapon()
        {
            currentWeapon.weapon.DeactivateWeapon();
            AttackPressUp();
            inventory.Reverse();
            currentWeapon.weapon.Initialize();
            onInventoryChange?.Invoke(inventory);
        }

        public void SwapWeapons()
        {
            currentWeapon.weapon.DeactivateWeapon();
            AttackPressUp();
            inventory.Reverse();
            currentWeapon.weapon.Initialize();
            onInventoryChange?.Invoke(inventory);
            onWeaponSwap?.Invoke();
        }
        #endregion
    }
}
