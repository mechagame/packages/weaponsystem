﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace WeaponSystem
{
    public class WSWeaponNameDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshPro nameDisplay;
        [SerializeField] private WSCollectibleWeapon weapon;
        [SerializeField] private Vector3 nameDisplayRotation = new Vector3(50, 0, 0);

        private void OnEnable()
        {
            nameDisplay.text = weapon.GetWeaponName;
        }

        private void LateUpdate()
        {
            nameDisplay.transform.rotation = Quaternion.Euler(nameDisplayRotation);
        }
    }
}
