﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSWeaponLooter : MonoBehaviour
    {
        [SerializeField] private WSAttackManager attackManager;
        [SerializeField] private WSCollectibleWeapon.WeaponType targetedWeaponType = WSCollectibleWeapon.WeaponType.range;

        private void OnTriggerEnter(Collider other)
        {
            if(!other.TryGetComponent(out WSCollectibleWeapon weaponDrop)) { return; }
            if(weaponDrop.GetWeaponType != targetedWeaponType) { return; }
            attackManager.AddWeapon(weaponDrop.GetWeaponName);
            weaponDrop.Collect();
        }
    }
}
