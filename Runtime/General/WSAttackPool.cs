﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSAttackPool : MonoBehaviour
    {
        [SerializeField] private List<WSWeaponData> weaponsPool = new List<WSWeaponData>();

        private Dictionary<string, WSWeaponData> weapons = new Dictionary<string, WSWeaponData>();
        public Dictionary<string, WSWeaponData> GetWeaponsPool => weapons;

        private void Awake()
        {
            CreateWeaponsList();
        }

        private void CreateWeaponsList()
        {
            for (int i = 0; i < weaponsPool.Count; i++)
            {
                weapons.Add(weaponsPool[i].GetWeaponName, weaponsPool[i]);
            }
        }

        public void FindWeapons()
        {
            weaponsPool.Clear();
            WSWeaponData[] _weaponDatas = GetComponentsInChildren<WSWeaponData>();
            for (int i = 0; i < _weaponDatas.Length; i++)
            {
                weaponsPool.Add(_weaponDatas[i]);
            }
        }
    }
}
