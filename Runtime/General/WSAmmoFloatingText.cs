﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace WeaponSystem
{
    public class WSAmmoFloatingText : MonoBehaviour
    {
        [SerializeField] private WSAttackManager attackManager;
        [SerializeField] private TextMeshPro text;
        [SerializeField] private float displayTime = 0.5f;
        private float remainingDisplayTime;

        private void OnEnable()
        {
            attackManager.onInventoryChange += InventoryChangeHandler;
        }

        private void OnDestroy()
        {
            attackManager.onInventoryChange -= InventoryChangeHandler;
        }

        private void Update()
        {
            if (remainingDisplayTime < 0) { return; }
            remainingDisplayTime -= Time.deltaTime;
            if (remainingDisplayTime > 0) { return; }
            text.enabled = false;
        }

        private void InventoryChangeHandler(List<WSInventoryWeapon> inventory)
        {
            int currentAmmo = inventory[0].ammo;
            if (currentAmmo != 0)
            {
                text.text = currentAmmo.ToString();
            }
            else if (inventory[1].ammo > 0)
            {
                text.text = "Switching Weapon!";
            }
            else
            {
                text.text = "No Ammo!";
            }
            text.enabled = true;
            remainingDisplayTime = displayTime;
        }
    }
}
