﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public class WSProjectileManager : MonoBehaviour
    {
        [SerializeField] private List<WSProjectileBehaviour> behaviours = new List<WSProjectileBehaviour>();

        private void OnEnable()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].SetGameObject(gameObject);
                behaviours[i].OnInitialize();
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnDisabled();
            }
        }

        private void Update()
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnUpdate();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            for (int i = 0; i < behaviours.Count; i++)
            {
                behaviours[i].OnCollide();
            }    
        }

        public void FindBehaviours()
        {
            behaviours.Clear();
            WSProjectileBehaviour[] projectileBehaviours = GetComponents<WSProjectileBehaviour>();
            for (int i = 0; i < projectileBehaviours.Length; i++)
            {
                behaviours.Add(projectileBehaviours[i]);
            }
        }

    }
}
