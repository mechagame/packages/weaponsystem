﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Forward Moving")]
    public class WSForwardMoving : WSProjectileBehaviour
    {
        [SerializeField] private Vector2 speedRange;
        private float movementSpeed;

        public override void OnInitialize()
        {
            movementSpeed = UnityEngine.Random.Range(speedRange.x, speedRange.y);
        }

        public override void OnUpdate()
        {
            pGameObject.transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
        }
    }
}
