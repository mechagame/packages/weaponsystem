﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Hide On Collision")]
    public class WSHideOnCollide : WSProjectileBehaviour
    {
        [SerializeField] private MeshRenderer mesh;

        public override void OnInitialize()
        {
            mesh.enabled = true;
        }

        public override void OnCollide()
        {
            mesh.enabled = false;
        }
    }
}
