﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Lifetime")]
    public class WSLifetime : WSProjectileBehaviour
    {
        [SerializeField] private float lifetime = 5f;
        public UnityEvent onLifeTimeEnd;
        private float remainingLifetime;

        public override void OnInitialize()
        {
            remainingLifetime = lifetime;
        }

        public override void OnUpdate()
        {
            remainingLifetime -= Time.deltaTime;
            if(remainingLifetime <= 0)
            {
                onLifeTimeEnd?.Invoke();
                pGameObject.SetActive(false);
            }
        }
    }
}
