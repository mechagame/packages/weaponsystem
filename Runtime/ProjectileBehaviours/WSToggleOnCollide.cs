﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Toggle on collision")]
    public class WSToggleOnCollide : WSProjectileBehaviour
    {
        [SerializeField] private GameObject itemToToggle;
        [SerializeField] private bool stateOnCollided;

        public override void OnInitialize()
        {
            itemToToggle.SetActive(!stateOnCollided);
        }

        public override void OnCollide()
        {
            itemToToggle.SetActive(stateOnCollided);
        }
    }
}
