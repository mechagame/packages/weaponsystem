﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public abstract class WSProjectileBehaviour : MonoBehaviour
    {
        protected GameObject pGameObject;
        public void SetGameObject(GameObject gameObject) => pGameObject = gameObject;

        public virtual void OnInitialize() { }
        public virtual void OnUpdate() { }
        public virtual void OnDisabled() { }
        public virtual void OnCollide() { }
    }
}
