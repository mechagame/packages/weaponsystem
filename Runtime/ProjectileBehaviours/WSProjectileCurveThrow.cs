﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Curve Throw")]
    public class WSProjectileCurveThrow : WSProjectileBehaviour
    {
        [SerializeField] AnimationCurve throwCurve;
        [SerializeField] bool deactivateOnLand = true;
        [SerializeField] float maxThrowHeight;
        [SerializeField] float minDistanceFromTarget = 0.05f;
        [SerializeField] Vector2 speedRange;
        [SerializeField] LayerMask groundMask;
        public UnityEvent onLanded;

        private Camera cam;
        private float currentSpeed;
        private Vector3 target;
        private float distanceToTarget;

        public override void OnInitialize()
        {
            if (cam == null) { cam = Camera.main; }
            currentSpeed = UnityEngine.Random.Range(speedRange.x, speedRange.y);
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 1000, groundMask))
            {
                target = new Vector3(hit.point.x, 0, hit.point.z);
                distanceToTarget = Vector3.Distance(gameObject.transform.position, target);
            }
            gameObject.transform.LookAt(target);
        }

        public override void OnUpdate()
        {
            MoveForward();
            UpdateYPosition();
        }

        private void UpdateYPosition()
        {
            Vector3 _currentGroundPosition = new Vector3(transform.position.x, 0, transform.position.z);
            float _remainingDistance = Vector3.Distance(_currentGroundPosition, target);
            if (_remainingDistance <= minDistanceFromTarget)
            {
                onLanded?.Invoke();
                if (deactivateOnLand) { gameObject.SetActive(false); }
            }
            float _currentY = throwCurve.Evaluate(1 - (_remainingDistance / distanceToTarget)) * maxThrowHeight;
            gameObject.transform.position = new Vector3(transform.position.x, _currentY, transform.position.z);
        }

        private void MoveForward()
        {
            gameObject.transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);
        }
    }
}
