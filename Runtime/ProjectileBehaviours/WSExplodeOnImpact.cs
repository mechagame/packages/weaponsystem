﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Explode On Impact")]
    public class WSExplodeOnImpact : WSProjectileBehaviour
    {
        [SerializeField] private GameObject explosion;

        public override void OnCollide()
        {
            GameObject newExplosion = WSPool.GetInstance.GetItemFromPool(explosion, transform.position, Quaternion.identity);
        }
    }
}
