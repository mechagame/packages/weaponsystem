﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Velocity Changing Move")]
    public class WSVelocityChangingMove : WSProjectileBehaviour
    {
        [SerializeField] private Vector2 baseSpeedRange;
        [SerializeField] private Vector2 timeBeforeVelocityChangeRange;
        [SerializeField] private Vector2 velocityChangeForceRange;

        private float remainingTimeBeforeVelocityChange;
        private float currentSpeed;
        private float velocityChangeForce;

        public override void OnInitialize()
        {
            currentSpeed = UnityEngine.Random.Range(baseSpeedRange.x, baseSpeedRange.y);
            remainingTimeBeforeVelocityChange = UnityEngine.Random.Range(timeBeforeVelocityChangeRange.x, timeBeforeVelocityChangeRange.y);
            velocityChangeForce = UnityEngine.Random.Range(velocityChangeForceRange.x, velocityChangeForceRange.y);
        }

        public override void OnUpdate()
        {
            Move();
            DecelerationCooldown();
            Decelerate();
        }

        private void DecelerationCooldown()
        {
            remainingTimeBeforeVelocityChange -= Time.deltaTime;
        }

        private void Decelerate()
        {
            if(remainingTimeBeforeVelocityChange > 0) { return; }

            currentSpeed = Mathf.MoveTowards(currentSpeed, 0, -velocityChangeForce * Time.deltaTime);

            if(currentSpeed <= 0) { gameObject.SetActive(false); }
        }

        private void Move()
        {
            gameObject.transform.Translate(Vector3.forward * currentSpeed * Time.deltaTime);
        }
    }
}
