﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Damage Dealer")]
    public class WSDamageDealer : MonoBehaviour
    {
        [SerializeField] private int damage;
        public int GetDamage => damage;
    }
}
