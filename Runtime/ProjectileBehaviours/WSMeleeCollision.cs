﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Melee Collision")]
    public class WSMeleeCollision : WSProjectileBehaviour
    {
        public static event Action onCollided;

        private void OnTriggerEnter(Collider other)
        {
            onCollided?.Invoke();
        }
    }
}
