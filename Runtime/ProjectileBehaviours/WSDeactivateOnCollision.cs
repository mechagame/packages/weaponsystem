﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Projectile Behaviour/Deactivate On Collision")]
    public class WSDeactivateOnCollision : WSProjectileBehaviour
    {
        [SerializeField] private float timeToDeactivate = 0.25f;

        public override void OnCollide()
        {
            Invoke("Deactivate", timeToDeactivate);
        }

        private void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}
