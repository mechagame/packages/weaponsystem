﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    public class WSDamageable : MonoBehaviour
    {
        [System.Serializable] private class OnDamageTakenEvent : UnityEvent<int> { }
        [SerializeField] private OnDamageTakenEvent onDamageTaken;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out WSDamageDealer damageDealer))
            {
                int damage = damageDealer.GetDamage;
                onDamageTaken?.Invoke(damage);
            }
        }
    }
}
