﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Circle Target")]
    public class WSTargetCircleBehaviour : WSWeaponBehaviour
    {
        [SerializeField] GameObject target;
        [SerializeField] float distanceFromGround = 0.5f;
        [SerializeField] LayerMask groundMask;
        private Camera cam;

        public override void OnInitialize()
        {
            if (cam == null) { cam = Camera.main; }
            target.SetActive(true);
        }

        public override void OnWeaponActive()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit, 1000, groundMask))
            {
                Vector3 position = hit.point;
                Vector3 newPosition = new Vector3(position.x, position.y + distanceFromGround, position.z);
                target.transform.position = newPosition;
            }
        }

        public override void OnDeactivate()
        {
            target.SetActive(false);
        }
    }
}
