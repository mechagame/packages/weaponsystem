﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Simple Shoot")]
    public class WSShootBehaviour : WSWeaponBehaviour
    {
        [SerializeField] private GameObject projectile;
        [SerializeField] private Transform shootOrigin;

        public override void OnExecute()
        {
            GameObject newProjectile = WSPool.GetInstance.GetItemFromPool(projectile, shootOrigin.position, shootOrigin.rotation);
        }
    }
}
