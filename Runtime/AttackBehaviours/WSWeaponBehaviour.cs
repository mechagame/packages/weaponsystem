﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    public abstract class WSWeaponBehaviour : MonoBehaviour
    {
        public virtual void OnInitialize() { }

        public virtual void OnExecute() { }

        public virtual void OnWeaponActive() { }

        public virtual void OnDeactivate() { }

        public virtual void OnInputHold() { }

        public virtual void OnInputRelease() { }
    }
}
