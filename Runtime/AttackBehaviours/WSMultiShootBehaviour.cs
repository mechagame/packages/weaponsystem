﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Multi Shoot")]
    public class WSMultiShootBehaviour : WSWeaponBehaviour
    {
        [SerializeField] GameObject projectile;
        [SerializeField] Transform shootOrigin;
        [SerializeField] List<float> angles = new List<float>();

        private void Reset()
        {
            angles.Add(0);
        }

        public override void OnExecute()
        {
            Vector3 baseRotation = shootOrigin.rotation.eulerAngles;
            for (int i = 0; i < angles.Count; i++)
            {
                Quaternion newRotation = Quaternion.Euler(baseRotation.x, baseRotation.y + angles[i], baseRotation.z);
                GameObject newProjectile = WSPool.GetInstance.GetItemFromPool(projectile, shootOrigin.position, newRotation);
            }
        }

    }
}
