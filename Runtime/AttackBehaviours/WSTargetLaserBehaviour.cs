﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Target Laser Behaviour")]
    public class WSTargetLaserBehaviour : WSWeaponBehaviour
    {
        [SerializeField] private Gradient lineGradient;
        [SerializeField] private Vector2 lineWidth;
        [SerializeField] private Material lineMaterial;
        [SerializeField] private Transform weaponPosition;
        [SerializeField] private LayerMask blockingLayers;
        [SerializeField] private float maxLaserRange;

        private LineRenderer lineRenderer;
        private Camera cam;
        private GameObject target;

        public override void OnInitialize()
        {
            if (lineRenderer == null) { CreateLine(); }
            if (cam == null) { cam = Camera.main; }
            if (target == null) { CreateTarget(); }

            lineRenderer.enabled = true;
            target.SetActive(true);
        }

        private void CreateTarget()
        {
            target = new GameObject();
            target.name = "Laser Target";
            target.transform.SetParent(weaponPosition);
            target.transform.localPosition = Vector3.zero;
        }

        private void CreateLine()
        {
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.colorGradient = lineGradient;
            lineRenderer.startWidth = lineWidth.x;
            lineRenderer.endWidth = lineWidth.y;
            lineRenderer.material = lineMaterial;
            lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }

        public override void OnWeaponActive()
        {
            Ray ray = new Ray(weaponPosition.position, weaponPosition.forward);
            target.transform.localPosition = new Vector3(0, 0, weaponPosition.localPosition.z + maxLaserRange);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, maxLaserRange, blockingLayers))
            {
                target.transform.position = hitInfo.point;
            }

            lineRenderer.SetPosition(0, weaponPosition.position);
            lineRenderer.SetPosition(1, target.transform.position);
        }

        public override void OnDeactivate()
        {
            if (lineRenderer == null) { CreateLine(); }
            if (target == null) { CreateTarget(); }
            target.SetActive(false);
            lineRenderer.enabled = false;
        }
    }
}
