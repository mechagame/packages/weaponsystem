﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Direct Placement")]
    public class WSDirectPositionBehaviour : WSWeaponBehaviour
    {
        [SerializeField] private GameObject projectile;
        [SerializeField] private float projectileHeight;
        [SerializeField] private LayerMask groundLayer;

        private Camera cam;

        public override void OnInitialize()
        {
            if(cam == null) { cam = Camera.main; }
        }

        public override void OnExecute()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit, groundLayer))
            {
                Vector3 itemPosition = new Vector3(hit.point.x, projectileHeight, hit.point.z);
                GameObject newItem = WSPool.GetInstance.GetItemFromPool(projectile, itemPosition, Quaternion.identity);
            }
        }
    }
}
