﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/InputHoldRelease")]
    public class WSHeldAttackBehaviour : WSWeaponBehaviour
    {
        public UnityEvent onInputHold;
        public UnityEvent onInputRelease;
        public override void OnInputHold()
        {
            onInputHold?.Invoke();
        }

        public override void OnInputRelease()
        {
            onInputRelease?.Invoke();
        }
    }
}
