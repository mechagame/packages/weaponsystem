﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Shotgun")]
    public class WSShotgunBehaviour : WSWeaponBehaviour
    {
        [SerializeField] GameObject projectile;
        [SerializeField] Transform shootOrigin;
        [SerializeField] Vector2Int numberOfShotsRange;
        [SerializeField] Vector2 shotAngleRange;

        public override void OnExecute()
        {
            int numberOfShots = UnityEngine.Random.Range(numberOfShotsRange.x, numberOfShotsRange.y);
            Vector3 baseRotation = shootOrigin.transform.rotation.eulerAngles;
            for (int i = 0; i < numberOfShots; i++)
            {
                float angleToAdd = UnityEngine.Random.Range(shotAngleRange.x, shotAngleRange.y);
                Quaternion newRotation = Quaternion.Euler(baseRotation.x, baseRotation.y + angleToAdd, baseRotation.z);
                GameObject newProjectile = WSPool.GetInstance.GetItemFromPool(projectile, shootOrigin.position, newRotation);
            }
        }
    }
}
