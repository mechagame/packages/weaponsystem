﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Toggle Behaviour")]
    public class WSToggleBehaviour : WSWeaponBehaviour
    {
        [SerializeField] private GameObject toggledItem;
        [SerializeField] private float toggleTime;
        public override void OnExecute()
        {
            Toggle();
            Invoke("Toggle", toggleTime);
        }

        private void Toggle()
        {
            toggledItem.SetActive(!toggledItem.activeSelf);
        }
    }
}
