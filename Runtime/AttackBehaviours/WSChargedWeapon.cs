﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WeaponSystem
{
    [AddComponentMenu("Weapon System/Weapon Behaviour/Charged Weapon")]
    public class WSChargedWeapon : WSWeaponBehaviour
    {
        [SerializeField] private float chargeTime;
        public UnityEvent onCharge;
        public UnityEvent onChargeCanceled;
        public UnityEvent onChargeEnded;

        private float remainingChargeTime;
        private bool isCharging;

        public override void OnInputHold()
        {
            isCharging = true;
            remainingChargeTime = chargeTime;
        }

        public override void OnInputRelease()
        {
            if (!isCharging) { return; }
            isCharging = false;
            remainingChargeTime = chargeTime;
            onChargeCanceled?.Invoke();
        }

        private void Update()
        {
            if (!isCharging) { return; }
            onCharge?.Invoke();
            remainingChargeTime -= Time.deltaTime;
            if(remainingChargeTime > 0) { return; }
            onChargeEnded?.Invoke();
            remainingChargeTime = chargeTime;
        }
    }
}
